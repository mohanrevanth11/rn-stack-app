import React, {Component} from 'react';
import {View} from 'react-native';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import reducers from './reducers';
import {AlbumHeader} from './components/common';
import LibraryList from './components/LibraryList';

const App = () => {    
    return(
        <Provider store={createStore(reducers)}>
            <View >
                <AlbumHeader headerName = 'Stack App' />                
                <LibraryList/>
            </View>
        </Provider>
    );
};

export default App;