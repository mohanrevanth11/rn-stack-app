import React, {Component} from 'react';
import {CardSection} from './common';
import {Text,StyleSheet,TouchableWithoutFeedback,View} from 'react-native';
import * as actions from '../actions'
import {connect} from 'react-redux';


class ListItem extends Component{

    renderDescription(){
        console.log(this.props);
        if(this.props.item.id == this.props.selectLibraryId){
            return(
                <Text>
                    {this.props.item.description}
                </Text>
            )
        }
    }

    render(){        
        return(
            <TouchableWithoutFeedback
                onPress = {() => this.props.selectLibrary(this.props.item.id)}>
                <View>
                    <CardSection>          
                        <Text style = {styles.titleStyle}>
                            {this.props.item.title}
                        </Text>
                    </CardSection>                    
                    {this.renderDescription()}
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const mapStateToProps = state =>{
    return {selectLibraryId : state.selectLibrary};
}

export default connect(mapStateToProps,actions) (ListItem);


const styles = StyleSheet.create({
    titleStyle:  {
        fontSize: 18,
        paddingLeft: 16,
        paddingBottom: 8
     }
 });